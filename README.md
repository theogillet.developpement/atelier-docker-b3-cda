# GESTION PRODUITS

## Prérequis
Cette application est compatible `PHP8.3` et a été testée avec une base de données `MySQL 8.3`.

## Installation
- Assurez vous d'avoir Docker d'installé sur votre machine
### Avec `docker run`
- Dans le dossier /database, exécutez `docker build -t gestion-produits-mysql -f Dockerfile .`
- Toujours dans le même dossier, lancez la commande `docker run --name mysql-container -e MYSQL_ROOT_PASSWORD=root -v ./gestion_produits.sql:/docker-entrypoint-initdb.d/gestion_produits.sql -d gestion-produits-mysql`
- Dans le dossier /www, exécutez `docker build -t gestion-produits-php -f Dockerfile .`
- Dans le dossier parent, lancez la commande `docker run --name php-apache-container -v ./www:/var/www/html --link mysql-container:db -p 8080:80 -d gestion-produits-php`

### Avec `docker compose`
- A la racine du projet, exécutez `docker compose up --build`

## Utilisation
- Rendez vous sur http://localhost:8080
- Connectez vous à l'application avec l'url adaptée avec les informations suivantes :
    - Login : `admin`
    - Mot de passe : `password`

## Fonctionnalités
L'application permet de :
- Lister les produits
- Afficher la fiche produit en lecture seule
- Ajouter des produits
- Modifier les produits
- Supprimer les produits
- Pour chaque produit, il est possible d'ajouter autant de photos que nécessaire
